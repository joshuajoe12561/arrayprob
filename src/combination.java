import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class combination {
    static int total = 0;

    private static void helper(int[] A, int data[], int start, int end, int index,int s1) {
        if (index == data.length) {
            int[] combination = data.clone();
            if (arraySum(combination)==s1/2){
                total+=1;
            }

        } else if (start <= end) {
            data[index] = A[start];

            helper(A, data, start + 1, end, index + 1,s1);
            helper(A, data, start + 1, end, index,s1);
        }
    }

    public static void generate(int[] A, int n,int s1) {

        for (int i = 0; i < n ; i++) {
            helper(A, new int[i], 0, n - 1, 0,s1);
        }


    }

public static int arraySum(int[] A) {
    int aSum = 0;
    for (int i = 0;i<A.length; i++) {
        aSum += A[i];
    }
    return aSum;
}
    public static void main(String[] args) {
        //{9,3,9,3,9,3,};
        int[] arr ={2, 5, 1, 3, 7, 2, 5, 1};
                //{2,5,1,3,7};
                //{12,3,3,0,3,3,2,7,9,4};
        // {2, 5, 1, 3, 7, 2, 5, 1, 3, 7};
        //{-1,-1,-1,-1,-1,-1};

        int s1=0;

        for (int i = 0; i < arr.length; i++) {
            s1+=arr[i];
        }
        generate(arr,arr.length,s1);

        if (total==1){
            System.out.println(1);
        }else
        System.out.println(total/2);

    }

}
